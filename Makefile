all: 
	docker system prune -a -f
	docker-compose build
	docker-compose up -d
	docker-compose run php composer install
	docker-compose run php bin/console doctrine:schema:update --force