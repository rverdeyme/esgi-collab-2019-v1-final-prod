
# ESGI collab 2019

## Requirements

**Docker :**

- Mac :
	-  [Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/)

- Windows :
	-  [Docker Desktop for Windows Professionnel](https://docs.docker.com/docker-for-windows/)
	-  [Docker Toolbox for other Windows](https://docs.docker.com/toolbox/toolbox_install_windows/)
- Linux :
	-  [Docker CE for Linux](https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/ubuntu/)

  

## Installation

  

Clone the repository :

    $ git clone https://gitlab.com/rverdeyme/esgi-collab-2019-v1-final-prod.git MixerProject

Go to the folder MixerProject :

    $ cd MixerProject
    
Launch serve :

    $ make all
    
