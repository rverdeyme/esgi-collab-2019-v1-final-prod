<?php

namespace App\Service;

class Calcul{
    public function addition ($nbr1, $nbr2) {
        return $nbr1 + $nbr2;
    }
    public function soustraction ($nbr1, $nbr2) {
        return $nbr1 - $nbr2;
    }
    public function multiplication ($nbr1, $nbr2) {
        return $nbr1 * $nbr2;
    }
    public function division ($nbr1, $nbr2) {
        return $nbr1 / $nbr2;
    }
}
