<?php

namespace App\Controller;

use App\Service\MixerService;
use App\Service\Calcul;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(MixerService $mixerService, Calcul $calcul)
    {
        $nbr1 = 1;
        $nbr2 = 2;

        if (isset($_GET['nbr1'])) {
            $nbr1 = $_GET['nbr1'];
        }
        if (isset($_GET['nbr2']) && $_GET['nbr2'] > 0) {
            $nbr2 = $_GET['nbr2'];
        }

        return $this->render('home/index.html.twig', [
            'nbr1' => $nbr1,
            'nbr2' => $nbr2,
            'addition' => $calcul->addition($nbr1, $nbr2),
            'soustraction' => $calcul->soustraction($nbr1, $nbr2),
            'multiplication' => $calcul->multiplication($nbr1, $nbr2),
            'division' => $calcul->division($nbr1, $nbr2),
            'items' => $mixerService->getData(),
        ]);
    }
}
